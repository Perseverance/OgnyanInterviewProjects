## Problem 1 - FizzBuzz
Write a program that prints the numbers from 1 to 100. But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz”.

## Problem 2 - DateFormat Converter
a) Write a function that converts user entered date formatted as M/D/YYYY to the following format YYYYMMDD. 

For example, it should convert user entered date "12/31/2014" to "20141231". 

b)Write another function that converts user entered date to Unix Timestamp

## Problem 3 - Gallery
Using Bootstrap - create a simple galery that has 8 images 
(use this as an image : http://via.placeholder.com/300x450)

The galery should normally have 2 rows with 4 images each.

The galery should be responsive. This means that

When view-ing it on a tablet (You can just shrink the browser window, not run it on table) it should display 4 rows with 2 images each.

When viewing it on a smartphone (Again just shrink a bit more) it should display all the images one below the other.

*Read this if you want to refresh your knowledge on the boostrap grid : https://v4-alpha.getbootstrap.com/layout/grid/#how-it-works*