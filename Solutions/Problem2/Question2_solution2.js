/**
 * Created by Ognyan on 30.9.2017 г..
 */
function solve(userDate) {
    var dateArray = userDate.split('/');
    var month = Number(dateArray[0]);
    var days = Number(dateArray[1]);
    var year = Number(dateArray[2]);
    //make date object
    var preFormDate = new Date(Date.UTC(year, month - 1, days)); // I use Date.UTC because depends on the timezone of the pc that run this code, it may return one day off. // month -1 because month is from 0 to 11
    function formattedData(preFormDate) {
        var formattedDate = ("" + preFormDate.getFullYear() + ("0" + (preFormDate.getMonth() + 1)).slice(-2) + ("0" + preFormDate.getDate()).slice(-2)); // here I check for months and days < 10 and attach leading zero
        console.log(formattedDate);
    }
    function timestampData(preFormDate) {
        console.log(preFormDate.getTime());
    }
    formattedData(preFormDate);
    timestampData(preFormDate);
}
solve("12/31/2014");
