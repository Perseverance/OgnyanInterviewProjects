/**
 * Created by Ognyan on 30.9.2017 г..
 */
//this method also can be done, but if user enters date like "02/31/2014" - the date is wrong! so I more like to use Date object
function convertDate(userDate) {
    var arr = userDate.split("/");
    var month = ("0" + arr[0]).slice(-2); //if month is <10 I attach leading zero
    var dayOfMonth = ("0" + arr[1]).slice(-2); // if day is <10 I attach leading zero
    var year = arr[2];
    return year + month + dayOfMonth;
}
console.log(convertDate("12/31/2014"));
function convertTimeStamp(userDate) {
    var arr = userDate.split("/");
    var month = ("0" + arr[0]).slice(-2);
    var dayOfMonth = ("0" + arr[1]).slice(-2);
    var year = arr[2];
    //make date object
    var timestamp = new Date(Date.UTC(Number(year), Number(month) - 1, Number(dayOfMonth))).getTime(); // month -1 because month is from 0 to 11
    return timestamp;
}
console.log(convertTimeStamp("12/31/2014"));
