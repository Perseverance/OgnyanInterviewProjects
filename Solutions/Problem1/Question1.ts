for(let i:number = 1; i<=100; i++) {

    let multiThree:boolean = i%3 == 0;
    let multiFive:boolean = i%5 == 0;
    console.log(multiThree ? multiFive ? "FizzBuzz" : "Fizz" : multiFive ? "Buzz" : i)
}
