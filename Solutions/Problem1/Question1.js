for (var i = 1; i <= 100; i++) {
    var multiThree = i % 3 == 0;
    var multiFive = i % 5 == 0;
    console.log(multiThree ? multiFive ? "FizzBuzz" : "Fizz" : multiFive ? "Buzz" : i);
}
